import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;


public class ChromeTest {
    private WebDriver driver;

    @BeforeTest
    public void profileSetUp () {
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver");
    }

    @BeforeMethod
    public void testsSetUp () {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://accounts.ukr.net/login?lang=ru");
    }

    @org.testng.annotations.Test
    public void sendEmail(){
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//input[@name='login']")).sendKeys("user_email@ukr.net");
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys("123456qwerty!");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        driver.findElement(By.xpath("//button[@class='button primary compose']")).click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//input[@name='toFieldInput']")).sendKeys("user_email@ukr.net");
        driver.findElement(By.xpath("//button[@class='button primary send']")).click();
        driver.findElement(By.xpath("//button[@class='button primary default']")).click();
    }

    @AfterMethod
    public void tearDown () {
        driver.quit();
    }
}
